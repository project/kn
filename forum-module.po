# LANGUAGE translation of Drupal (modules/forum.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from file: forum.module,v 1.243.2.10 2005/06/19 08:44:25 dries
#
msgid ""
msgstr ""
"Project-Id-Version: drupal-kn translations\n"
"POT-Creation-Date: 2005-06-30 11:06+0200\n"
"PO-Revision-Date: 2005-08-11 16:24+0530\n"
"Last-Translator: HPN <hpnadig@gmail.com>\n"
"Language-Team: Kannada <kannada.l10n@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Poedit-Language: Kannada\n"
"X-Poedit-Country: INDIA\n"
"X-Poedit-SourceCharset: utf-8\n"

#: modules/forum.module:15
msgid "<p>This is a list of existing containers and forums that you can edit. Containers hold forums and, in turn, forums hold threaded discussions. Both containers and forums can be placed inside other containers and forums. By planning the structure of your containers and forums well, you make it easier for users to find a topic area of interest to them.</p>"
msgstr ""

#: modules/forum.module:17
msgid "<p>Containers help you organize your forums. The job of a container is to hold, or contain, other forums that are related. For example, a container named \"Food\" might hold two forums named \"Fruit\" and \"Vegetables\". Containers are usually placed at the top (root) level of your forum but you can also place a container within another container or forum.</p>"
msgstr ""

#: modules/forum.module:19
msgid "<p>A forum holds threaded topic discussions that are related. For example, a forum named \"Fruit\" might contain topic discussions titled \"Apples\" and \"Bananas\". You can place a forum into a container, another forum, or at the top (root) level of your forum.</p>"
msgstr ""

#: modules/forum.module:21
msgid "Enables threaded discussions about general topics."
msgstr "ಸಾಮಾನ್ಯ ವಿಷಯಗಳ ಬಗ್ಗೆ ಮಾತು ಕಥೆಯನ್ನು ಅನುವು ಮಾಡಿಕೊಡುತ್ತದೆ."

#: modules/forum.module:23
msgid "This is where you can configure system-wide options for how your forums act and display."
msgstr ""

#: modules/forum.module:25
msgid "A forum is a threaded discussion, enabling users to communicate about a particular topic."
msgstr ""

#: modules/forum.module:33;414
msgid "forum topic"
msgstr "ಫಾರಮ್ ವಿಷಯ"

#: modules/forum.module:135
msgid "Are you sure you want to delete the forum %name?"
msgstr "%name ಹೆಸರಿನ ಫಾರಮ್ ಅನ್ನು ಅಳಿಸಲು ಹೋಗುತ್ತಿದ್ದೀರಿ. ಖಚಿತಪಡಿಸಿ. "

#: modules/forum.module:137
msgid "Deleting a forum or container will delete all sub-forums as well. This action cannot be undone."
msgstr "ಫಾರಮ್ ಒಂದನ್ನು ಅಳಿಸಿದರೆ ಎಲ್ಲ ಸಬ್ - ಫಾರಮ್ ಗಳೂ ಅಳಿಸಿಹೋಗುತ್ತವೆ. ಈ‌ ಕಾರ್ಯ ಪೂರ್ವಸ್ಥಿತಿಗೆ ಹಿಂದಿರುಗಿಸಲಾಗುವುದಿಲ್ಲ. "

#: modules/forum.module:150
msgid "Container name"
msgstr "ಕಂಟೇಯ್ನರ್ "

#: modules/forum.module:150
msgid "The container name is used on the forum listing page to identify a group of forums."
msgstr ""

#: modules/forum.module:151
msgid "The description can provide additional information about the forum grouping."
msgstr ""

#: modules/forum.module:154
msgid "In listings, the heavier terms (with a larger weight) will sink and the lighter terms will be positioned nearer the top."
msgstr ""

#: modules/forum.module:172
msgid "Forum name"
msgstr "ಫಾರಮ್ ಹೆಸರು "

#: modules/forum.module:172
msgid "The name is used to identify the forum."
msgstr "ಈ ಹೆಸರು ಫಾರಮ್ ಅನ್ನು ಗುರುತಿಸಲು ಉಪಯೋಗಿಸಲಾಗುತ್ತದೆ. "

#: modules/forum.module:173
msgid "The description can be used to provide more information about the forum, or further details about the topic."
msgstr ""

#: modules/forum.module:176
msgid "In listings, the heavier (with a higher weight value) terms will sink and the lighter terms will be positioned nearer the top."
msgstr ""

#: modules/forum.module:237;441
msgid "edit container"
msgstr "ಕಂಟೇಯ್ನರನ್ನು ಬದಲಾಯಿಸಿ"

#: modules/forum.module:240;444
msgid "edit forum"
msgstr "ಫಾರಮ್ ಬದಲಾಯಿಸಿ"

#: modules/forum.module:246
msgid "There are no existing containers or forums. You may add some on the <a href=\"%container\">add container</a> or <a href=\"%forum\">add forum</a> pages."
msgstr ""

#: modules/forum.module:288
msgid "Forum icon path"
msgstr "ಫಾರಮ್ ಐಕಾನ್ ಪಾತ್ "

#: modules/forum.module:288
msgid "The path to the forum icons.  Leave blank to disable icons. Don't add a trailing slash.  Default icons are available in the \"misc\" directory. You may use images of whatever size you wish, but it is recommended to use 15x15 or 16x16. "
msgstr ""

#: modules/forum.module:290
msgid "Hot topic threshold"
msgstr "ಹಾಟ್ ಟಾಪಿಕ್ ಮಿತಿ "

#: modules/forum.module:290
msgid "The number of posts a topic must have to be considered hot."
msgstr "ಟಾಪಿಕ್ ಒಂದಕ್ಕೆ ಇರಬಹುದಾದ ಸೆರ್ಪಡೆಗಳನ್ನು 'ಹಾಟ್' ಎಂದು ಪರಿಗಣಿಸಬೇಕು"

#: modules/forum.module:292
msgid "Topics per page"
msgstr "ಪುಟವೊಂದಕ್ಕೆ ವಿಷಯಗಳು"

#: modules/forum.module:292
msgid "The default number of topics displayed per page; links to browse older messages are automatically being displayed."
msgstr ""

#: modules/forum.module:293
msgid "Posts - most active first"
msgstr "ಪೋಸ್ಟ್ ಗಳು - ಚಟುವಟಿಕೆಯುಳ್ಳದ್ದು ಮೊದಲಿಗೆ "

#: modules/forum.module:293
msgid "Posts - least active first"
msgstr "ಪೋಸ್ಟ್ ಗಳು - ಚಟುವಟಿಕೆಯುಳ್ಳದ್ದು ಮೊದಲಿಗೆ  "

#: modules/forum.module:294
msgid "Default order"
msgstr "ಡೀಫಾಲ್ಟ್ ಕ್ರಮ "

#: modules/forum.module:294
msgid "The default display order for topics."
msgstr "ವಿಷಯಗಳ ಡಿಫಾಲ್ಟ್ ಕ್ರಮ"

#: modules/forum.module:317;333
msgid "Active forum topics"
msgstr "ಚಟುವಟಿಕೆಯುಳ್ಳ ಫಾರಮ್ ವಿಷಯಗಳು  "

#: modules/forum.module:318;342
msgid "New forum topics"
msgstr "ಹೊಸ ಫಾರಮ್ ವಿಷಯಗಳು "

#: modules/forum.module:322
msgid "Number of topics"
msgstr "ಒಟ್ಟು ವಿಷಯಗಳು"

#: modules/forum.module:352
msgid "Read the latest forum topics."
msgstr "ಇತ್ತೀಚೆಗಿನ ಫಾರಮ್ ವಿಷಯಗಳನ್ನು ಓದಿ. "

#: modules/forum.module:396
msgid "previous forum topic"
msgstr "ಹಿಂದಿನ ಫಾರಮ್ ವಿಷಯ"

#: modules/forum.module:400
msgid "next forum topic"
msgstr "ಮುಂದಿನ ಫಾರಮ್ ವಿಷಯ"

#: modules/forum.module:417;422
msgid "forums"
msgstr "ಫಾರಮ್ ಗಳು "

#: modules/forum.module:430
msgid "add container"
msgstr "ಕಂಟೇಯ್ನರ್ ಸೇರಿಸಿ"

#: modules/forum.module:433
msgid "add forum"
msgstr "ಫಾರಮ್ ಸೇರಿಸಿ"

#: modules/forum.module:493
msgid "The item %forum is only a container for forums. Please select one of the forums below it."
msgstr ""

#: modules/forum.module:534
msgid "Leave shadow copy"
msgstr "ನೆರಳಿನ ಕಾಪಿಯೊಂದನ್ನು ಬಿಟ್ಟು ಹೋಗಿ. "

#: modules/forum.module:534
msgid "If you move this topic, you can leave a link in the old forum to the new forum."
msgstr ""

#: modules/forum.module:564
msgid "%time ago<br />by %author"
msgstr "%time ಹಿಂದೆ<br /> %author ನಿಂದ"

#: modules/forum.module:657
msgid "Topic"
msgstr "ವಿಷಯ "

#: modules/forum.module:659
msgid "Created"
msgstr "ಸೃಷ್ಟಿಸಿದ್ದು"

#: modules/forum.module:660
msgid "Last reply"
msgstr "ಕೊನೆಯ ಉತ್ತರ"

#: modules/forum.module:742
msgid "Warning"
msgstr "ಎಚ್ಚರಿಕೆ"

#: modules/forum.module:788
msgid "My forum discussions."
msgstr "ನನ್ನ ಫಾರಮ್ ಮಾತುಕಥೆಗಳು "

#: modules/forum.module:791
msgid "Active forum discussions."
msgstr "ಚಟುವಟಿಕೆಯುಳ್ಳ ಫಾರಮ್ ಮಾತುಕಥೆಗಳು  "

#: modules/forum.module:795
msgid "Post new forum topic."
msgstr "ಹೊಸ ಫಾರಮ್ ವಿಷಯವನ್ನು ಸೇರಿಸಿ"

#: modules/forum.module:798
msgid "You are not allowed to post a new forum topic."
msgstr "ಹೊಸ ಫಾರಮ್ ವಿಷಯವನ್ನು ಪೋಸ್ಟ್ ಮಾಡಲು ನಿಮಗೆ ಅನುಮತಿಯಿಲ್ಲ. "

#: modules/forum.module:801
msgid "<a href=\"%login\">Login</a> to post a new forum topic."
msgstr "ಹೊಸ ವಿಷಯವೊಂದನ್ನು ಪೋಸ್ಟ್ ಮಾಡಲು <a href=\"%login\">ಲಾಗಿನ್</a> ಆಗಿ."

#: modules/forum.module:819
msgid "No forums defined"
msgstr "ಫಾರಮ್ ಗಳು ಯಾವೂ ಇಲ್ಲ, "

#: modules/forum.module:836
msgid "Forum"
msgstr "ಫಾರಮ್ "

#: modules/forum.module:836
msgid "Topics"
msgstr "ವಿಷಯಗಳು"

#: modules/forum.module:836
msgid "Posts"
msgstr "ಪೋಸ್ಟ್ ಗಳು"

#: modules/forum.module:869;904
msgid "%a new"
msgstr "%a ಹೊಸ "

#: modules/forum.module:897
msgid "This topic has been moved"
msgstr "ಈ ವಿಷಯ ಬೇರೆಡೆಗೆ ವರ್ಗಾಯಿಸಲಾಗಿದೆ."

#: modules/forum.module:57
msgid "create forum topics"
msgstr "ಫಾರಮ್ ವಿಷಯಗಳನ್ನು ಸೃಷ್ಟಿಸುವುದು"

#: modules/forum.module:57
msgid "edit own forum topics"
msgstr "ತನ್ನ ಫಾರಮ್ ಟಾಪಿಕ್ ಗಳನ್ನು ಬದಲಾವಣೆ ಮಾಡುವುದು"

#: modules/forum.module:57
msgid "administer forums"
msgstr "ಫಾರಮ್ ನಿರ್ವಹಣೆ"

#: modules/forum.module:0
msgid "forum"
msgstr "ಫಾರಮ್ "

